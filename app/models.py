from datetime import datetime
import hashlib
from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app, request, url_for
from flask_login import UserMixin, AnonymousUserMixin
from . import db, login_manager
from app.exceptions import ValidationError


class Permission:
    FOLLOW = 0x01
    COMMENT = 0x02
    WRITE_ARTICLES = 0x04
    MODERATE_COMMENTS = 0x08
    ADMINISTER = 0x80


class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('User', backref='role', lazy='dynamic')

    @staticmethod
    def insert_roles():
        roles = {
            'User': (Permission.FOLLOW |
                     Permission.COMMENT |
                     Permission.WRITE_ARTICLES, True),
            'Moderator': (Permission.FOLLOW |
                          Permission.COMMENT |
                          Permission.WRITE_ARTICLES |
                          Permission.MODERATE_COMMENTS, False),
            'Administrator': (0xff, False)
        }
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.permissions = roles[r][0]
            role.default = roles[r][1]
            db.session.add(role)
        db.session.commit()

    def __repr__(self):
        return '<Role %r>' % self.name


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    password_hash = db.Column(db.String(128))
    confirmed = db.Column(db.Boolean, default=False)
    name = db.Column(db.String(64))
    location = db.Column(db.String(64))
    about_me = db.Column(db.Text())
    member_since = db.Column(db.DateTime(), default=datetime.utcnow)
    last_seen = db.Column(db.DateTime(), default=datetime.utcnow)
    avatar_hash = db.Column(db.String(32))

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role is None:
            if self.email == current_app.config['DATENIUM_ADMIN']:
                self.role = Role.query.filter_by(permissions=0xff).first()
            if self.role is None:
                self.role = Role.query.filter_by(default=True).first()
        if self.email is not None and self.avatar_hash is None:
            self.avatar_hash = hashlib.md5(
                self.email.encode('utf-8')).hexdigest()

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_confirmation_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'confirm': self.id})

    def confirm(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        return True

    def generate_reset_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.id})

    def reset_password(self, token, new_password):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('reset') != self.id:
            return False
        self.password = new_password
        db.session.add(self)
        return True

    def generate_email_change_token(self, new_email, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'change_email': self.id, 'new_email': new_email})
    
    
    def generate_auth_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'id': self.id}).decode('ascii')
    
    
    @staticmethod
    def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return None
        return User.query.get(data['id'])

    def change_email(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('change_email') != self.id:
            return False
        new_email = data.get('new_email')
        if new_email is None:
            return False
        if self.query.filter_by(email=new_email).first() is not None:
            return False
        self.email = new_email
        self.avatar_hash = hashlib.md5(
            self.email.encode('utf-8')).hexdigest()
        db.session.add(self)
        return True

    def can(self, permissions):
        return self.role is not None and \
            (self.role.permissions & permissions) == permissions

    def is_administrator(self):
        return self.can(Permission.ADMINISTER)

    def ping(self):
        self.last_seen = datetime.utcnow()
        db.session.add(self)

    def gravatar(self, size=100, default='identicon', rating='g'):
        if request.is_secure:
            url = 'https://secure.gravatar.com/avatar'
        else:
            url = 'http://www.gravatar.com/avatar'
        hash = self.avatar_hash or hashlib.md5(
            self.email.encode('utf-8')).hexdigest()
        return '{url}/{hash}?s={size}&d={default}&r={rating}'.format(
            url=url, hash=hash, size=size, default=default, rating=rating)
        
        
    def to_json(self):
        json_user = {
            'url' : url_for('api.get_project', id=self.id, _external=True),
            'username': self.username,
            'email': self.email,
            'member_since': self.member_since,
            'last_seen': self.last_seen,
            'projects': url_for('api.get_user_projects', id=self.id, _external=True),
        }
        return json_user
    
    def __repr__(self):
        return '<User %r>' % self.username


class AnonymousUser(AnonymousUserMixin):
    def can(self, permissions):
        return False

    def is_administrator(self):
        return False

login_manager.anonymous_user = AnonymousUser

class Project(db.Model):
    __tablename__ = 'projects'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), unique=True)
    url = db.Column(db.String(1000))
    requirements = db.Column(db.String(2000))
    pages_count = db.Column(db.Integer, default=0)
    configured = db.Column(db.Boolean(), default=False)
    deleted = db.Column(db.Boolean(), default=False)
    billed = db.Column(db.Boolean(), default=False)
    download_filetype = db.Column(db.String(256))
    #working = db.Column(db.Boolean(), default=True)
    created_on = db.Column(db.DateTime(), index=True, default=datetime.utcnow)
    updated_on = db.Column(db.DateTime(), index=True, default=datetime.utcnow, onupdate=datetime.utcnow)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    
    
    def to_json(self):
        json_post = {
            'url': url_for('api.get_project', id=self.id, _external=True),
            'requirements': self.requirements,
            'sites': self.url,
            'author': url_for('api.get_user', id=self.author_id, _external=True),
            'downloadTypes': self.download_filetype,
        }
        return json_post
    
        
    
    @staticmethod
    def from_json(json_post):
        project = json_post.get('project_name')
        requirements = json_post.get('requirements')
        sites = json_post.get('sites')
        download_type = json_post.get('download_type')
        if (project or requirements or sites or download_type) is None or (project or requirements or sites or download_type) == '':
            raise ValidationError('Project cannot be created, some or all values are missing.')
        else:
            file_type = []
            if 'csv' in download_type.lower():
                file_type.append('CSV')
            elif 'sqlite' in download_type.lower():
                file_type.append('Sqlite')
            elif 'text' in download_type.lower():
                file_type.append('Text')
            else:
                raise ValidationError('download_type format error. please use this format, these are the only options available. >> download_type=["CSV", "Sqlite", "Text"] << ')
            return Project(name=project, requirements=requirements, url=sites, download_type=file_type)

    def __repr__(self):
        return '<Project %r>' % self.name



class Events(db.Model):
    __tablename__ = 'events'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))
    date = db.Column(db.Date)
    time = db.Column(db.String(10))
    street = db.Column(db.String(50))
    zipcode = db.Column(db.String(10))
    city = db.Column(db.String(30))
    country = db.Column(db.String(30))
    description = db.Column(db.String(2000))
    poster = db.Column(db.String(100))
    deleted = db.Column(db.Boolean(), default=False)
    created_on = db.Column(db.DateTime(), index=True, default=datetime.utcnow)
    updated_on = db.Column(db.DateTime(), index=True, default=datetime.utcnow, onupdate=datetime.utcnow)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    

    def get_url(self):
        return url_for('api.get_events', id=self.id, _external=True)
    
    def to_json(self):
        json_post = {
            'url': self.get_url(),
            'name' = self.name,
            'date' = self.date,
            'time' = self.time,
            'street' = self.street,
            'zipcode' = self.zipcode,
            'city' = self.city,
            'country' = self.country,
            'description' = self.description,
            'poster' = self.poster,
            'author': url_for('api.get_user', id=self.author_id, _external=True),
        }
        return json_post
    
        
    
    @staticmethod
    def from_json(json_post):
        'url': url_for('api.get_events', id=self.id, _external=True),
        'name' = self.name,
        'date' = self.date,
        'time' = self.time,
        'street' = self.street,
        'zipcode' = self.zipcode,
        'city' = self.city,
        'country' = self.country,
        'description' = self.description,
        'poster' = self.poster,
        'author': url_for('api.get_user', id=self.author_id, _external=True),



        project = json_post.get('project_name')
        requirements = json_post.get('requirements')
        sites = json_post.get('sites')
        download_type = json_post.get('download_type')
        if (project or requirements or sites or download_type) is None or (project or requirements or sites or download_type) == '':
            raise ValidationError('Project cannot be created, some or all values are missing.')
        else:
            file_type = []
            if 'csv' in download_type.lower():
                file_type.append('CSV')
            elif 'sqlite' in download_type.lower():
                file_type.append('Sqlite')
            elif 'text' in download_type.lower():
                file_type.append('Text')
            else:
                raise ValidationError('download_type format error. please use this format, these are the only options available. >> download_type=["CSV", "Sqlite", "Text"] << ')
            return Project(name=project, requirements=requirements, url=sites, download_type=file_type)

    def __repr__(self):
        return '<Project %r>' % self.name






@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


