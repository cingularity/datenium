from flask import jsonify, request, current_app, url_for
from . import api
from ..models import User, Project, Role, Permission
from .. import db
from .errors import bad_request
from .decorators import permission_required




@api.route('/users/<int:id>', methods=['GET'])
@permission_required(Permission.ADMINISTER)
def get_user(id):
    user = User.query.get_or_404(id)
    return jsonify(user.to_json())


@api.route('/users/<int:id>/projects/', methods=['GET'])
@permission_required(Permission.ADMINISTER)
def get_user_projects(id):
    user = User.query.get_or_404(id)
    projects = Project.query.filter_by(author_id=user.id).all()
    return jsonify({
        'posts': [project.to_json() for project in projects],
    })


@api.route('/add-user/', methods=['POST'])
def add_user():
    email = request.json.get('email')
    username = request.json.get('username')
    password = request.json.get('password')
    password2 = request.json.get('password2')
    if username is None or password is None or email is None or password2 is None:
        return bad_request('Missing arguments') # missing arguments
    if password != password2:
        return bad_request('The password must match.')
    if User.query.filter_by(username = username).first() is not None:
        return bad_request('User already exists.') # existing user
    user = User(username = username, password=password, email=email)
    #user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    return jsonify(user.to_json()), 201, {'Location': None}