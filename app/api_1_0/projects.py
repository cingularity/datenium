from flask import jsonify, request, g, abort, url_for, current_app
from .. import db
from ..models import Project, Permission
from . import api
from .decorators import permission_required
from .errors import forbidden



@api.route('/projects/')
def get_projects():
    projects = Project.query.all()
    return jsonify({ 'projects': [project.to_json() for project in projects] })





@api.route('/project/<int:id>')
def get_project(id):
    project = Project.query.get_or_404(id)
    return jsonify(project.to_json())
    

