from flask import g, jsonify, request
from flask_httpauth import HTTPBasicAuth
from ..models import User, AnonymousUser, db
from . import api
from .errors import unauthorized, forbidden, bad_request

auth = HTTPBasicAuth()


@auth.verify_password
def verify_password(email_or_token, password):
    if email_or_token == '':
        g.current_user = AnonymousUser()
        return True
    if password == '':
        g.current_user = User.verify_auth_token(email_or_token)
        g.token_used = True
        return g.current_user is not None
    user = User.query.filter_by(email=email_or_token).first()
    if not user:
        return False
    g.current_user = user
    g.token_used = False
    return user.verify_password(password)


@auth.error_handler
def auth_error():
    return unauthorized('Invalid credentials')


@api.before_request
@auth.login_required
def before_request():
    if not g.current_user.is_anonymous and \
            not g.current_user.confirmed:
        return forbidden('Unconfirmed account')
    
    
@api.route('/token', methods=['POST'])
def get_token():
    if g.current_user.is_anonymous or g.token_used:
        return unauthorized('Invalid credentials')
    return jsonify({'token': g.current_user.generate_auth_token(expiration=3600), 'expiration': 3600})

'''
@api.route('/add-user/', methods=['POST'])
def add_user():
    email = request.json.get('email')
    username = request.json.get('username')
    password = request.json.get('password')
    password2 = request.json.get('password2')
    if username is None or password is None or email is None or password2 is None:
        return bad_request('Missing arguments') # missing arguments
    if password != password2:
        return bad_request('The password must match.')
    if User.query.filter_by(username = username).first() is not None:
        return bad_request('User already exists.') # existing user
    user = User(username = username, password=password, email=email)
    #user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    return jsonify({'username': user.username, 'email': user.email}), 201, {'Location': None}
'''