from flask_wtf import Form
from wtforms import (StringField, SubmitField, ValidationError, BooleanField, 
        HiddenField)
from wtforms.validators import Required, Optional
from flask_babel import lazy_gettext



class AllProjectForm(Form):
    project_id = HiddenField()
    release = SubmitField(lazy_gettext('Release'), validators=[Optional()])
    deactivate = SubmitField(lazy_gettext('Deactivate'), validators=[Optional()])
    configure = SubmitField(lazy_gettext('Configure'), validators=[Optional()])
    
    

class UndeleteForm(Form):
    project_id = HiddenField()
    undelete = SubmitField(lazy_gettext('Undelete'), validators=[Optional()])
    permadelete = SubmitField(lazy_gettext('Delete Permanently'), validators=[Optional()])    