from flask import render_template, redirect, request, url_for, flash
from flask_login import login_user, login_required, logout_user, current_user
from flask_babel import gettext as _

from ..models import Project
from . import admin
from ..decorators import admin_required, permission_required
from .. import db
from ..email import send_email
from .forms import AllProjectForm, UndeleteForm



@admin.route("/")
@admin_required
def index():
    new_projects = db.session.query(Project).filter_by(configured=None).all()
    return render_template('admin/index.html', projects = new_projects)


@admin.route("/all-project", methods=['GET', 'POST'])
@admin_required
def all_project():
    ''' All projects except the deleted ones '''
    all_project = db.session.query(Project).filter_by(deleted=False).order_by(Project.updated_on.desc()).all()
    pending_proj = db.session.query(Project).filter_by(configured=False).\
        filter_by(deleted=False).order_by(db.desc(Project.created_on)).all()
    form = AllProjectForm()
    if form.validate_on_submit():
        project_id = form.data['project_id']
        if form.data['release'] is True:
            project = db.session.query(Project).filter_by(id=project_id).first()
            project.configured = True
            flash("release Pressed")
        if form.data['deactivate'] is True:
            project = db.session.query(Project).filter_by(id=project_id).first()
            project.configured = False
            flash("deactivate is pressed.")
        db.session.commit()
        return redirect(url_for('.all_project'))
    return render_template('admin/new-projects.html', 
                           projects=all_project, pending_projects=pending_proj, 
                           form=form)
    

@admin.route("/deleted-project", methods=['GET', 'POST'])
@admin_required
def deleted_project():
    ''' All deteled project listed here. '''
    del_projects = db.session.query(Project).filter_by(deleted=True).order_by(Project.updated_on.desc()).all()
    form = UndeleteForm()
    if form.validate_on_submit():
        project_id = form.data['project_id']
        if form.data['undelete'] is True:
            project = db.session.query(Project).filter_by(id=project_id).filter_by(deleted=True).first()
            project.deleted = False
            project.configured = False
            flash('Undeleting')
        if form.data['permadelete'] is True:
            flash('deleting permanently')
        db.session.commit()
        return redirect(url_for('.deleted_project'))
    return render_template('admin/deleted-project.html', projects=del_projects, form=form)

