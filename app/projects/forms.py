from flask_wtf import Form
from wtforms import StringField, SubmitField, SelectMultipleField
from wtforms.validators import Required, Length, Email, Regexp, EqualTo
from wtforms import ValidationError
from flask_babel import lazy_gettext
from ..models import Project
from wtforms.widgets import TextArea, CheckboxInput, ListWidget





class ProjectForm(Form):
    name = StringField('Project Name', validators=[Required(), Length(1, 256), 
                    Regexp('^[A-Za-z][A-Za-z0-9_]*$', 0,
                           'Project name must have only letters (A-Z, a-z), '
                           'numbers or underscores')], 
                           render_kw={"placeholder": "Project name must have only letters (A-Z, a-z, _)"})
    url = StringField('Sites', validators=[Required(), Length(1, 1000)], 
                            render_kw={"placeholder": "site.com, site.de, site.net"})
    requirements = StringField('Requirements', validators=[Required(), Length(1, 2000)], widget=TextArea(), 
                               render_kw={"placeholder": "Detailed description of the data required from the site."})
    
    download_filetype = SelectMultipleField('Download file type <small>(Use Ctrl to select multiple)</small>', 
                            choices=[('CSV','CSV format'),
                                     ('Sqlite','Sqlite format'), 
                                     ('Text','Text format')], default = ('CSV','CSV format'))
                            #option_widget=CheckboxInput(), widget = ListWidget(html_tag='ul', prefix_label=False))
    Submit = SubmitField('Create Project')
    
    
    

    
class ProjectButton(Form):
    download = SubmitField(lazy_gettext('Download'))
    update = SubmitField(lazy_gettext('Update'))
    delete = SubmitField(lazy_gettext('Delete'))
