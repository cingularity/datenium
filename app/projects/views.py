from flask import render_template, redirect, request, url_for, \
                flash, send_from_directory
from flask_login import login_user, logout_user, login_required, current_user
from flask_babel import gettext as _
import os
from pathlib import Path

from . import projects
from .. import db
from ..models import Project, User
from ..email import send_email
from .forms import ProjectForm, ProjectButton
from config import basedir


'''
@projects.route('/')
def index():
    return render_template('projects/index.html')
'''

@projects.route('/dashboard', methods=['GET', 'POST'])
@login_required
def dashboard():
    form = ProjectForm()
    projects = db.session.query(Project).filter_by(author_id=current_user.id).order_by(db.desc(Project.updated_on)).all()
    if form.validate_on_submit():
        if projects is not None:
            '''Check only projects from the same user that are not deleted.'''
            project_name = [project_name.name for project_name in projects]
            deleted_project = []
            for project in projects:
                if project.deleted is False:
                    deleted_project.append(project.name)
            if form.name.data in project_name and deleted_project:
                flash('Please choose a new name. It already exists.')
            else:
                url = form.url.data # no sanitize # sanitize url to add http://
                name = form.name.data
                new_project=Project(name=name, url=url,
                    requirements=form.requirements.data, author_id=current_user.id, download_filetype=form.download_filetype.data)
                db.session.add(new_project)
                db.session.commit()
                #latest_project = db.session.query(Project).filter_by(name=name).order_by(db.desc(Project.created_on)).first()
                #scrap_template(name, url, latest_project.id, latest_project.download_filetype) # create template for the project
                scrap_template(name)
                flash('Your new Project has been added. You shall receive an email as' \
                    ' soon as we are done with the configuration.')
            return redirect(url_for('.dashboard'))
    return render_template('projects/dashboard.html', projects=projects, form=form)


def sanitize_url(url):
    if 'http://' or 'https://' in url[0:8]:
        url = url
    else:
        url = 'http://' + url
    return url

def scrap_template(name):
    latest_project = db.session.query(Project).filter_by(name=name).first()
    dir_name = os.path.join(basedir, 'static', "ScrapProjects/{}/".format(latest_project.id))
    os.makedirs(os.path.join(dir_name, name), exist_ok=True) # directory for the codes
    if 'CSV' in latest_project.download_filetype:
        os.makedirs(os.path.join(dir_name, 'csv'), exist_ok=True) # directory for csv files
    if 'Sqlite' in latest_project.download_filetype:
        os.makedirs(os.path.join(dir_name, 'sqlite'), exist_ok=True) # directory for sqlite files    
    if 'Text' in latest_project.download_filetype:
        os.makedirs(os.path.join(dir_name, 'text'), exist_ok=True) # directory for text files
        
    
    
    
@projects.route("/delete", methods=['POST'])
@login_required
def delete():
    if request is not None:
        project_id = int(request.form['project_to_delete_id'])
        project = db.session.query(Project).filter_by(id=project_id).first()
        if 'delete' in request.form: 
            #project = db.session.query(Project).filter_by(id=project_id).first()
            #name = project.name
            #db.session.delete(project)
            #db.session.commit()
            project.deleted = True
            flash(_('%s has been permanently deleted.' % project.name))
        elif 'update' in request.form: 
            flash('Update pressed. Project id is {}'.format(project_id))
    return redirect(url_for('.dashboard'))


'''
@projects.route("/download", method=['GET'])
@login_required
def download():
    flash(('id : {}, user : {}, filetype : {}').format(request.args.get('id'), request.args.get('user'), request.args.get('filetype')))

'''

@projects.route('/download/<int:project_id>/<filetype>')
@login_required
def download(project_id, filetype):
    project = db.session.query(Project).filter_by(id=project_id).first()
    filetype = filetype.lower()
    if project.author_id == current_user.id:
        dir = os.path.join(basedir, 'static', "ScrapProjects/{}/".format(project_id), filetype)
        if 'text' in filetype:
            file = '{}.{}'.format(project.name, 'txt')
        else:
            file = '{}.{}'.format(project.name, filetype.lower())
        if Path(os.path.join(dir, file)).is_file():
            return send_from_directory(dir, file, mimetype=None, as_attachment=True, attachment_filename=file)
    return redirect(url_for('.dashboard'))
