from urllib.request import urlopen, Request
from urllib.error import HTTPError
from bs4 import BeautifulSoup



class ScrapBase(object):
    def __init__(self, link, encoding='en-US'):
        self.scrap_url = link
        user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
        headers={'User-Agent':user_agent,} 
        request = Request(self.scrap_url, None, headers)
        # read html or return error
        try:
            html = urlopen(request)
        except HTTPError as e:
            print("error", e)
            return None 
        # try creating object or return error       
        try:
            scrapObj = BeautifulSoup(html.read(), 'html.parser')            
        except AttributeError as e:
            print("error", e)
            return None
    
        #assign object
        self.scrapObj = scrapObj
        
    def get_object(self):
        return self.scrapObj